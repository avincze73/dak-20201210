using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace dn_currency_exchange.Controllers
{
    [ApiController]
    public class CurrencyExchangeController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        [Route("[controller]/from/{from}/to/{to}")]
        public ActionResult<CurrencyExchangeModel> Get(string from, string to)
        {
            string host = Environment.GetEnvironmentVariable("HOSTNAME") == null ? "LOCAL" : Environment.GetEnvironmentVariable("HOSTNAME");
            return new CurrencyExchangeModel{Host = host, FromCurrency = "USD", ToCurrency = "HUF", ExchangeRate = 303};
        }

        [HttpGet]
        [Route("[controller]/fromm/{from}/too/{to}")]
        public ActionResult<String> Gett(string from, string to)
        {
            if (from == "USD"){
                return "300";
            } else if (from == "EUR") {
                return "400";
            } else {
                return "500";
            }

        }
    }
}
