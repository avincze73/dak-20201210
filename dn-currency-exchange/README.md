# Web Application

## Deploying the application
```bash
docker-compose build
docker-compose push


kubectl apply -f dn-currency-exchange/k8s/k8s.yaml
kubectl logs POD_ID -f

kubectl run -i --tty busybox --image=busybox --rm --restart=Never -- sh
uname -a
echo $PATH
wget -qO- http://dn-currency-exchange:8003/currencyexchange/fromm/USD/too/HUF
```

