import time
from datetime import datetime


def main():
    while True:
        dateTimeObj = datetime.now()
        print(dateTimeObj, " ubuntu runs containers v2! ")
        time.sleep(5)


if __name__ == "__main__":
    main()