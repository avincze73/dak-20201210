package hu.sprint.ts.echo;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class EchoController {

    @Autowired
    private InstanceInformationService service;

    @GetMapping(path = "/")
    public String imUpAndRunning() {
        log.info("{healthy:true,app:echo}");
        return "{healthy:true,app:echo}";
    }

    @GetMapping(path = "/echo")
    public String echoFromInstance() {
        log.info("Echo from instance v1 " + service.retrieveInstanceInfo());
        return "Echo from instance v1 " + service.retrieveInstanceInfo();
    }

}
