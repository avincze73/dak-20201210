package hu.sprint.ts.ems.repository;

import hu.sprint.ts.ems.entity.Employee;
import org.springframework.data.jpa.repository.JpaRepository;

public interface EmployeeRepository extends JpaRepository<Employee, Long> {

}
