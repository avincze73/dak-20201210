package hu.sprint.ts.greeting;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Slf4j
public class GreetingController {

    @Autowired
    private InstanceInformationService service;

    @GetMapping(path = "/")
    public String imUpAndRunning() {
        log.info("{healthy:true,app:greeting}");
        return "{healthy:true,app:greeting}";
    }

    @GetMapping(path = "/greetings")
    public String greetingsFromInstance() {
        log.info("Greetings from instance v2 " + service.retrieveInstanceInfo());
        return "Greetings from instance v2 " + service.retrieveInstanceInfo();
    }

//    @GetMapping(path = "/greetings")
//    public Greeting greeting() {
//        log.info("Greetings from v1");
//        return new Greeting("Greetings from v1");
//    }
//
//    @GetMapping(path = "/greetings/{name}")
//    public Greeting greetingPathVariable(@PathVariable String name) {
//        return new Greeting(String.format("Greetings, %s", name));
//    }
}
