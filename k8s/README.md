# Kubernetes samples

## Two containers in one pod

```bash
kubectl apply -f k8s/01-two-containers-pod.yaml
```

## Pod presets 

- Pod presets are used to inject kubernetes resources like Secrets, ConfigMaps, Volumes and Environment variables.

```bash
kubectl apply -f k8s/02-podpreset.yaml
```

