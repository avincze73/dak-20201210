# Ingress

## Setup Ingress
```bash
sudo apt install snapd
sudo snap install helm --classic

kubectl create namespace ingress-basic

helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx

helm install nginx-ingress ingress-nginx/ingress-nginx \
    --namespace ingress-basic \
    --set controller.replicaCount=2 \
    --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set controller.admissionWebhooks.patch.nodeSelector."beta\.kubernetes\.io/os"=linux

kubectl --namespace ingress-basic get services -o wide -w nginx-ingress-ingress-nginx-controller

#kubectl get ingress --namespace ingress-basic

kubectl apply -f ingress/aks-helloworld-one.yaml --namespace ingress-basic
kubectl apply -f ingress/aks-helloworld-two.yaml --namespace ingress-basic
kubectl apply -f ingress/hello-world-ingress.yaml
```


[Főoldal](../README.md)
