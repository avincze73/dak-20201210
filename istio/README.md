# Istio

## Setup Istio
> use the client shell
```bash
# Specify the Istio version that will be leveraged throughout these instructions
ISTIO_VERSION=1.7.3
curl -sL "https://github.com/istio/istio/releases/download/$ISTIO_VERSION/istioctl-$ISTIO_VERSION-linux-amd64.tar.gz" | tar xz
#curl -sL "https://github.com/istio/istio/releases/download/$ISTIO_VERSION/istioctl-$ISTIO_VERSION-osx.tar.gz" | tar xz
./istioctl operator init
kubectl get all -n istio-operator
./istioctl profile dump default
kubectl create ns istio-system
kubectl apply -f istio.aks.yaml 
kubectl get all -n istio-system
kubectl logs -n istio-operator -l name=istio-operator -f

./istioctl dashboard grafana
./istioctl dashboard prometheus
./istioctl dashboard jaeger
./istioctl dashboard kiali
```

## Deploy with istio
```bash
kubectl apply -f istio/01-ts-greeting.yaml
# Enabling istio side cars in each pods
kubectl label namespace default istio-injection=enabled

# Specify the container inside the pod
kubectl logs ts-greeting-656666dfdf-vshs8 -f
kubectl logs ts-greeting-656666dfdf-vshs8 ts-greeting -f
```

## Http gateway and virtual service
```bash
kubectl get svc --namespace istio-system
kubectl apply -f istio/02-http-gateway.yaml
kubectl apply -f istio/03-virtualservice.yaml
kubectl get svc --namespace istio-system

```

## Multiple deployments
```bash
kubectl apply -f istio/04-multiple-deployments.yaml
kubectl get pods
watch -n 0.1 curl 52.149.108.130:8001/greetings
```

## Mirroring with istio
```bash
kubectl apply -f istio/05-mirroring.yaml
watch -n 0.1 curl 20.71.5.214/greetings
```

## Canary deployment with istio
```bash
kubectl apply -f istio/06-canary.yaml
watch -n 0.1 curl 20.71.5.214/greetings
```


## Kiali
```bash
./istioctl dashboard kiali
```

## Grafana
```bash
./istioctl grafana
```

## Prometheus
```bash
./istioctl dashboard prometheus
```

## Jaeger
```bash
./istioctl dashboard jaeger
```

[Főoldal](../README.md)
