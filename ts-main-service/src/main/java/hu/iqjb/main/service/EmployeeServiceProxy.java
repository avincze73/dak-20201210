package hu.iqjb.main.service;

import org.springframework.cloud.netflix.ribbon.RibbonClient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "ts-employee-service")
@RibbonClient(name = "ts-employee-service")
public interface EmployeeServiceProxy {
    @GetMapping("/employee")
    String retrieveFromEmployeeService();
}