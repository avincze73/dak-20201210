package hu.iqjb.main.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainController {


    @Autowired
    private EmployeeServiceProxy employeeServiceProxy;

    @GetMapping("/call-employee")
    public String callEmployee(){
       return employeeServiceProxy.retrieveFromEmployeeService();
    }

}
