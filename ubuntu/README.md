# Setup Ubuntu 20.04

## Post installation tasks
```bash
adduser sprint
usermod -aG sudo sprint
sudo apt-get update
sudo apt upgrade -y
sudo apt install -y curl git vim maven
sudo ufw enable
sudo ufw allow ssh
sudo ufw reload
sudo systemctl status ufw.service
sudo ufw status

sudo apt install -y zsh
zsh --version
sudo usermod -s $(which zsh) sprint
sudo chsh -s $(which zsh)
echo $SHELL
sh -c "$(curl -fsSL https://raw.githubusercontent.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"

mvn -v
git config --global credential.helper store

```

## Installing .net core
```bash
wget https://packages.microsoft.com/config/ubuntu/20.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-sdk-3.1

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y aspnetcore-runtime-3.1

sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y dotnet-runtime-3.1

```

## Installing docker
```bash
sudo apt-get install -y apt-transport-https ca-certificates curl gnupg-agent software-properties-common
sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo apt-get update
sudo apt install -y docker-ce docker-ce-cli containerd.io docker-compose
sudo usermod -aG docker sprint
sudo systemctl start docker
sudo systemctl enable docker

docker -v
docker info
docker images
docker ps -a
```

## Installing kubectl
```bash
sudo apt-get update && sudo apt-get install -y apt-transport-https gnupg2 curl
curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | sudo apt-key add -
echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee -a /etc/apt/sources.list.d/kubernetes.list
sudo apt-get update
sudo apt install -y kubectl

kubectl version
# Explain the missing Server Version
```

[Főoldal](../README.md)
