package hu.sprint.ts.currency.conversion;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "currency-exchange", url = "${CURRENCY_EXCHANGE_URI:http://localhost}:${CURRENCY_EXCHANGE_PORT:8084}/")
public interface CurrencyExchangeServiceProxy {
    @GetMapping("/ts-currency-exchange/from/{from}/to/{to}")
    CurrencyExchange retrieveExchangeValue(@PathVariable("from") String from,
                                                        @PathVariable("to") String to);
}