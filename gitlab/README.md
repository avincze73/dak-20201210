# Kubernetes with Gitlab



```bash
kubectl get secrets
kubectl get secret default-token-qm78v -o jsonpath="{['data']['ca\.crt']}" | base64 --decode
kubectl apply -f 01-gitlab-admin-service-account.yaml
kubectl apply -f 02-gitlab-admin-cluster-role-binding.yaml
kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep gitlab-admin | awk '{print $1}')

#staging

```



[Főoldal](../README.md)
