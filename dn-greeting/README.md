# Basics of Kubernetes

## Building the application
```bash
dotnet build
docker-compose -f  dn-greeting/docker-compose.yaml build
docker-compose -f  dn-greeting/docker-compose.yaml push
```



## Imperative deployment
```bash
kubectl create deployment dn-greeting --image=avincze73/dn-greeting
kubectl get deployments
kubectl get pods
kubectl expose deployment/dn-greeting --type="LoadBalancer" --port 8001
kubectl describe services/dn-greeting
kubectl scale --replicas=3 deployment/dn-greeting


# kubectl run dn-greeting --image=avincze73/dn-greeting:v2 --restart=Never
kubectl autoscale deployment dn-greeting --max=10 --min=3 --cpu-percent=30
kubectl top pods


kubectl api-resources -o wide
kubectl api-versions 
kubectl explain deployments --api-version=apps/v1
kubectl explain ingress --api-version=extensions/v1beta1
```

## Generating and playing with yaml files
```bash
kubectl get deployments dn-greeting -o wide
kubectl get deployments dn-greeting -o yaml
kubectl get svc dn-greeting -o yaml > service.yaml

# Generators
# run, create, expose commands use helper templates called generators
# dry-run only print the object that would be sent, without sending it.
kubectl apply -f dn-greeting/k8s/01-pod.yaml --dry-run=client
kubectl run busybox --image=busybox --dry-run=client -o yaml --restart=Never > busybox.yaml



# Each resource type in Kubernetes has a secification
kubectl create deployment my-dep --image nginx --dry-run=client -o yaml
kubectl create job my-job --image nginx --dry-run=client -o yaml
kubectl expose deployment my-dep -port 80 --dry-run=client -o yaml



kubectl delete all -l app=dn-greeting
kubectl get all
kubectl apply -f merged.yaml
curl 35.193.174.32:8001/greetings-from-instance
kubectl diff -f merged.yaml
kubectl apply -f merged.yaml
kubectl get deployments
curl 35.193.174.32:8001/greetings
watch -n 0.1 curl 35.193.174.32:8080/greetings
```

## Elements of kubernetes configuration
```bash
kubectl api-resources -o wide
kubectl api-versions 
kubectl explain services.spec
kubectl explain services.spec.type
kubectl explain services --recursive
kubectl explain deployments --api-version=apps/v1
kubectl explain ingress --api-version=extensions/v1beta1
```

### Example 1.
- Create an nginx server on kubernetes cluster.
- Create a redis server on kubernetes cluster.

## Elements of Kubernernetes Abstraction
- __Pod__: it is a unit of one or more containers on one node.
- __Controller__: It creates or updates pods and other objects. Can be _Deployment, ReplicaSet, StatefulSet, DaemonSet, Job, CronJob, etc._
- __Service__: It is a network endpoint connecting to a pod. 
- __Namespace__: It is a group of objects in the cluster 
- __Secret__
- __ConfigMap__


## Pod
```bash
# deploying the pod
kubectl apply -f dn-greeting/k8s/01-pod.yaml
kubectl get pods
kubectl describe pod dn-greeting
kubectl get svc
kubectl describe svc dn-greeting
kubectl logs dn-greeting -f
kubectl get pods --show-labels

# service discovery
kubectl run -it --tty busybox --image=busybox --restart=Never --rm  -- sh
nslookup dn-greeting
wget -q -O- http://dn-greeting:8001/greetings

# clean up
kubectl delete pod dn-greeting
kubectl delete svc dn-greeting
# or
kubectl delete -f dn-greeting/k8s/01-pod.yaml
```

## ReplicationController

Kubernetes replicaset is very similar to deployment but the later is better to use. 
- Scaling vertically (allocate more memory/cpu)
- Scaling horizontally (increase the number of pods)

Stateless applications can be scaled horizontally
- A stateless application does not have state and does not use any store to keep state.
- RDBMS applications are stateful, the state is stored in db files.
- Web applications are stateless if the state is not stored inside the container. Session replication is done outside of the container with session state solutions (redis, hazelcast).
- Stateful applications can use volumes to store state.

Scaling can be done with the help of Replication Controller
- It controls the specified number of pod replicas at a time.
- The lifecycle of  replicated pods are handled by kubernetes replication controller.
- In case of a single pod  replication controller should be used.


```bash
kubectl apply -f dn-greeting/k8s/02-replication.yaml
kubectl get rc
kubectl get pods
kubectl describe pod dn-greeting-qdmc5
watch -n 0.1 curl http://20.50.173.16:8001/greetings

# What is happening with pods?
kubectl delete pod dn-greeting-qdmc5

kubectl scale --replicas=5 -f dn-greeting/k8s/02-replication.yaml
kubectl scale --replicas=10 -f dn-greeting/k8s/02-replication.yaml
kubectl delete rc dn-greeting
# clean up
kubectl delete -f dn-greeting/k8s/02-replication.yaml
```


## Deployment

Deployment objects allow applications to be deployed and updated.
- Deployment defines the state (e.g. number of replicas) of the application.
- Deployment object can be easier to use.
- With Deployment object we can create, update, roll back, pause the deployments.

With deployment object we can
- deploy an app
- update an app
- do rolling updates
- roll back to previous version

```bash
kubectl apply -f dn-greeting/k8s/03-deployment.yaml
kubectl get deployments
kubectl get rs
kubectl get pods --show-labels
kubectl rollout status deployment/dn-greeting
kubectl set image deployment/dn-greeting dn-greeting=avincze73/dn-greeting:v2
watch -n 0.1 curl http://20.54.187.4:8001/greetings
kubectl rollout status deployment/dn-greeting
kubectl rollout history deployment/dn-greeting
kubectl rollout undo deployment/dn-greeting
kubectl rollout status deployment/dn-greeting
kubectl edit deployment/dn-greeting
kubectl rollout undo  deployment/dn-greeting --to-revision=1
kubectl rollout undo  deployment/dn-greeting --to-revision=2
# clean up
kubectl delete -f dn-greeting/k8s/03-deployment.yaml
```



## Service

Pods can not be accessed directly but through a Service
- pods are very dynamic: can be scaled up and down

- Service is the interface of a pod to other service or end-user.
- _kubectl expose_ creates a service

Service endpoints of a pod
- __ClusterIP__: only reachable from within the cluster
- __NodePort__: this port is reachable externally (30000-32767)
- __LoadBalancer__: created by cloud providers
Service provides dns name to pods

```bash
# from the previous example
kubectl apply -f dn-greeting/k8s/03-deployment.yaml
kubectl describe svc dn-greeting
```


## Labels
Labels are arbitary key/value pairs that can be attached to kubernetes objects
- Multiple labels can be added to an object
- With labels we can use label selectors
- A node can be labeled
- Resources use label selectors to link resource dependencies 
- With label selectors we can control which pod goes to which node

```bash
kubectl get nodes --show-labels
kubectl get pods
kubectl apply -f dn-greeting/k8s/04-node-selector.yaml
kubectl describe pod dn-greeting-7d68f78d58-s9jzt
kubectl label nodes aks-agentpool-27054194-vmss000000 hardware=avg-spec
kubectl get pods
kubectl label node aks-agentpool-27054194-vmss000000 hardware-
kubectl delete -f dn-greeting/k8s/04-node-selector.yaml
```



## Health check
There is a situation when the pod is alive but the application does not work anymore.
- Liveness probe indicates that the container is in running phase. If it is failed the container will be restarted.
- Readiness indicates that the container is ready to serve the request. If the check fails the pods IP address will be removed from the Service so it will not serv any request. It is checked at startup.

```bash
# liveness
kubectl apply -f dn-greeting/k8s/05-healthcheck.yaml
kubectl describe pod podid
kubectl edit deployment dn-greeting
kubectl delete -f dn-greeting/k8s/05-healthcheck.yaml

# readiness
kubectl apply -f dn-greeting/k8s/06-healthcheck.yaml
kubectl describe pod podid
kubectl edit deployment dn-greeting
kubectl delete -f dn-greeting/k8s/06-healthcheck.yaml
```

## Pod state
Pods have a STATUS field. 
- _kubectl get pods_ command
- __Pending__: the pod is accepted but is not running
- __Running__: at least one container is running
- __Succeeded__: All containers in the pod has been terminaated successfully.		
- __Failed__: all containers in the pod was terminated and at least one container returned a failure code.
- __Unknown__: the state of the pod can not be determined.

Pod condition
- _kubectl describe pod <name of the pod>_
- __PodScheduled__: the pod was scheduled to a node
- __Ready__: the pod can serve requests
- __Initialized__: the initialization container was started
- __Unschedulable__: the pod can not be scheduled
- __ContainersReady__: all containers in the pod are ready



## Pod lifecycle
```bash
kubectl apply -f dn-greeting/k8s/07-lifecycle.yaml
kubectl exec -it ts-busybox-774df7dd5c-nkw4s -- tail -f /timing
kubectl delete -f dn-greeting/k8s/07-lifecycle.yaml
```


## Multiple containers in one pod
```bash
kubectl apply -f k8s/13-two-container-pod.yaml
kubectl describe pod two-containers
kubectl exec -it two-containers nginx-container -- /bin/bash
apt-get update
apt-get install curl procps
ps aux
```


## Service discovery
> Multiple containers in a pod can connect to each other directly (without service object) using localhost:port notation
```bash
kubectl exec -it POD -- mysql -u root -p
kubectl run -it --tty --rm busybox --image=busybox --restart=Never  -- sh
nslookup service-name
telnet service-name port
```

## External DNS
```bash
PUBLIC_IP_ID=$(az network public-ip list --query "[?ipAddress=='20.50.175.31'].id" -o tsv)
az network dns zone create --resource-group KubernetesTraining2020  --name tsm.kubernetes.sprint.com
az network dns record-set a add-record --resource-group KubernetesTraining2020 --record-set-name @ --zone-name tsm.kubernetes.sprint.com  --ipv4-address 1.1.1.1
az network dns record-set a update --name @ --resource-group KubernetesTraining2020  --zone-name tsm.kubernetes.sprint.com --target-resource $PUBLIC_IP_ID
az network dns zone show --resource-group KubernetesTraining2020 --name tsm.kubernetes.sprint.com  --query nameServers
az network public-ip update --ids $PUBLIC_IP_ID --dns-name tsm.kubernetes.sprint.com
```

## Autoscaling
- Pods are queried in order to investigate the utilization of them
- 100m = 100 millicpu, millicores
- 100m = 0.1, 10% of a cpu core


```bash
kubectl apply -f dn-greeting/k8s/08-autoscaling.yml
kubectl top pod
kubectl run -i -t load-generator --image=busybox -- sh
while true;do wget -q -O- http://dn-greeting:8001/greetings;done
kubectl delete -f dn-greeting/k8s/08-autoscaling.yml
```


## Affinity/antiaffinity

- Using nodeSelector we can schedule the pod on specific nodes. 
- The affinity and anti-affinity allows us to use  complex scheduling.
- The rules of affinity and anti-affinity are not strick rules so the scheduler can execute the pod even if the rules are not met.
- A rule can take  into consideration other pod labels: two different pods are not on the same node.
- One possibility is __node affinity__. It is very similar to nodeSelector.
- The other possibility is __pod affinity or anti-affinity__.
- The affinity and anti-affinity rules are applied during scheduling phase.

Node affinity and anti-affinity
- requiredDuringSchedulingIgnoredDuringExecute (like node selector)
- preferredDuringSchedulingIgnoredDuringExecute (hint only)


```bash
# node affinity
kubectl get nodes
kubectl describe node aks-agentpool-27054194-vmss00000
kubectl apply -f dn-greeting/k8s/09-node-affinity.yaml
kubectl describe pod dn-greeting-5c7c46f7bf-q2xqv
kubectl label node aks-agentpool-27054194-vmss000000 env=dev
kubectl label node aks-agentpool-27054194-vmss000001 team=engineering-project1
kubectl delete -f dn-greeting/k8s/09-node-affinity.yaml
```

Interpod affinity and anti-affinity

- It allows the influence of scheduling based on the labels of other running pods.
- The rules are connected to specific namespace.
- requiredDuringSchedulingIgnoredDuringExecute
- preferredDuringSchedulingIgnoredDuringExecute
- One possibile use case for pod affinity is co-location. Your app and redis should be scheduled on the same node.
- topologyKey refers to node label
- The new pod will only be scheduled on nodes that have the same topologyKey value as the current pod has.
- Anti-affinity gives the ability that a pod is only scheduled once on a node. E.g. you do not want to schedule two pods on the same node if the pod labels match.


```bash
# pod affinity
kubectl apply -f dn-greeting/k8s/10-pod-affinity.yaml
kubectl get pods -o wide
kubectl scale --replicas=3 deployment/pod-affinity-2
kubectl delete -f dn-greeting/k8s/10-pod-affinity.yaml
```

```bash
# pod anti-affinity
kubectl apply -f dn-greeting/k8s/11-pod-anti-affinity.yaml
kubectl get pods -o wide
kubectl delete -f dn-greeting/k8s/11-pod-anti-affinity.yaml

```

## Taints and tolerations

Toleration is the opposite of node affinity.
- Toleration disallows pods to run on specific nodes.
- General use-case for taints is to make sure that a pod is not scheduled on the master node.
- We can add a new taint to a node with the following command: _kubectl taint nodes node1 key==value:NoSchedule_
- No pods will be scheduled on _node1_
- taints are preferences not strict rules. 

Taint values
- NoSchedule: hard requirement
- PreferNoSchedule: soft requirement

If the taint is applied after the pods are running, the pods will not be evicted
- unless the _NoExecute_ taint is applied

Use cases
- Taint nodes are dedicated for a team e.g.
- In case of a special hardware we can taint them to avoid executing applications on those nodes.

```bash
# taints
kubectl get nodes -o wide
kubectl taint nodes aks-agentpool-27054194-vmss000000 type=specialnode:NoSchedule
kubectl get nodes -o wide
kubectl get pods -o wide
kubectl delete pod tolerations-1-56bbc6748c-nnhlz
kubectl get pods -o wide
kubectl apply -f dn-greeting/k8s/12-tolerations.yaml
kubectl taint nodes aks-agentpool-27054194-vmss000001 testkey=testvalue:NoExecute
kubectl delete -f dn-greeting/k8s/12-tolerations.yaml
kubectl taint nodes aks-agentpool-27054194-vmss000001 testkey-
kubectl taint nodes aks-agentpool-27054194-vmss000000 type-
```




## one service with two deployments
```bash

kubectl delete all -l app=dn-greeting
kubectl get all
kubectl apply -f k8s/14-merged.yaml
kubectl get all -o wide
curl 35.193.174.32:8080/greetings
kubectl diff -f merged.yaml
kubectl apply -f merged.yaml
kubectl get deployments
brew install watch
curl 35.193.174.32:8001/greetings
watch -n 1 curl 35.193.174.32:8001/greetings
```



[Főoldal](../README.md)