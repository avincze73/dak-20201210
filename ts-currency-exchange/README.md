# Currency-exchange

## Volumes

It is a story about stateful applications.
- By definition volumes in kubernetes allows us to store date outside the cluster.
- It protects us from data lost.
- In stateless application the state can be stored in external services.
- Using Persistent Volumes we can attach a volume to the container that exists in case of container stop.
- There are many volume providers: ebs storage, google disk, nfs, azure disk, local volume
- Using volumes the application can have state in the cluster.

## Deploying mysql service to Azure
```bash
kubectl apply -f k8s/01-mysql-data-persistentvolumeclaim.yaml
kubectl apply -f k8s/02-db-deployment.yaml
kubectl apply -f k8s/03-db-service.yaml
kubectl get pods
kubectl describe deployment db
kubectl logs db-b8f689f96-v6qkk -f
```


## Checking mysql service
```bash
kubectl exec -it db-ID -- mysql -u tsuser -ptitkos123

# from mysql client
show databases;
exit;
```


## Using configmap
 ```bash
kubectl create configmap ts-currency-config --from-literal=MYSQL_DATABASE=ts
kubectl get configmap ts-currency-config
kubectl describe configmap/ts-currency-config

#Edit and add MYSQL_USER
kubectl edit configmap/ts-currency-config
kubectl describe configmap/ts-currency-config

# OR
kubectl apply -f ts-currency-exchange/k8s/06-configmap.yaml


 ```


## Using secrets
 ```bash
kubectl create secret generic ts-currency-secrets --from-literal=MYSQL_ROOT_PASSWORD=Titkos123
kubectl get secret/ts-currency-secrets
kubectl describe secret/ts-currency-secrets
echo -n "titkos123" | base64
kubectl edit secret/ts-currency-secrets
# Add the base64 encoded string to secret
kubectl delete -f db-deployment.yaml
kubectl apply -f k8s/04-db-deployment-with-configmap-and-secret.yaml
 ```

## Adding keys to configmap
 ```bash
#Edit and add MYSQL_USER
kubectl edit configmap/ts-currency-config
kubectl describe configmap/ts-currency-config
 ```

## Deploying ts-currency-exchange
```bash
mvn clean package
docker-compose build
docker-compose push

kubectl apply -f k8s/05-currency-exchange.yaml
kubectl logs nn-currency-exchange-86555b6894-g4dqg -f
```

##Using busybox to get into pods
```bash
kubectl run -i --tty busybox --image=busybox --restart=Never -- sh
uname -a
echo $PATH
wget -qO- 10.0.0.177:8084/nn-currency-exchange
```

[Főoldal](../README.md)