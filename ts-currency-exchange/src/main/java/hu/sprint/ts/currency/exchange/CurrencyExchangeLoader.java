package hu.sprint.ts.currency.exchange;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;

@Component
public class CurrencyExchangeLoader implements CommandLineRunner {

    @Autowired
    private CurrencyExchangeRepository repository;


    @Override
    public void run(String... args) throws Exception {

        if(!repository.findAll().isEmpty()){
            return;
        }

        CurrencyExchange currencyExchange = new CurrencyExchange("USD", "HUF", new BigDecimal(320));
        repository.save(currencyExchange);

        currencyExchange = new CurrencyExchange("EUR", "HUF", new BigDecimal(380));
        repository.save(currencyExchange);

        currencyExchange = new CurrencyExchange("CHF", "HUF", new BigDecimal(400));
        repository.save(currencyExchange);

    }
}
