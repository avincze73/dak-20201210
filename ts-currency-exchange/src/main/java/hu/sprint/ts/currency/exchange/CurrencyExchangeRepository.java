package hu.sprint.ts.currency.exchange;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrencyExchangeRepository extends JpaRepository<CurrencyExchange, Long> {
    CurrencyExchange findTopByFromCurrencyAndToCurrency(String fromCurrency, String toCurrency);
}
