package hu.sprint.ts.currency.exchange;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@RestController
@Slf4j
public class CurrencyExchangeController {

    @Autowired
    private CurrencyExchangeRepository repository;

    @Autowired
    private InstanceInformationService instanceInformationService;


    @GetMapping("/")
    public String imHealthy() {
        return "{healthy:true}";
    }

    @GetMapping("/ts-currency-exchange/from/{from}/to/{to}")
    @ResponseStatus(HttpStatus.OK)
    public CurrencyExchange retrieveCurrencyExchange(@PathVariable String from, @PathVariable String to,
                                                     @RequestHeader Map<String, String> headers) {

        printAllHeaders(headers);

        CurrencyExchange currencyExchange = repository.findTopByFromCurrencyAndToCurrency(from, to);

        log.info("{} {} {}", from, to, currencyExchange);

        if (currencyExchange == null) {
            throw new RuntimeException("Unable to find data to convert " + from + " to " + to);
        }

        currencyExchange.setEnvironmentInfo(instanceInformationService.retrieveInstanceInfo());

        return currencyExchange;
    }

    private void printAllHeaders(Map<String, String> headers) {
        headers.forEach((key, value) -> {
            log.info(String.format("Header '%s' = %s", key, value));
        });
    }
}
