package hu.iqjb.employee.service;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "bean")
@Getter
@Setter
public class EmployeeConfig {

    private String message = "Message from backend is: %s <br/> Services : %s";

}
