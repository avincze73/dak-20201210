using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace dn_ems.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        // GET api/values
        // [HttpGet]
        // public ActionResult<IEnumerable<string>> Get()
        // {
        //     var data = new StringBuilder();
        //     using (var context = new EmployeeContext())
        //     {
        //         var employees = context.Employee;

        //         foreach (var employee in employees)
        //         {
        //             data.AppendLine($"firstName: {employee.firstName}");
        //             data.AppendLine($"lastName: {employee.lastName}");

        //             Console.WriteLine(data.ToString());
        //         }
        //     }
        //     string host = Environment.GetEnvironmentVariable("HOSTNAME");
        //     string mysqlHost = Environment.GetEnvironmentVariable("MYSQL_HOST");
        //     return new string[] { "value1", "value2" , host, mysqlHost, data.ToString()};
        // }


        public ActionResult<IEnumerable<Employee>> Get()
        {
            var data = new StringBuilder();
            Employee[] employeeArray;
            using (var context = new EmployeeContext())
            {
                var employees = context.Employee;

                foreach (var employee in employees)
                {
                    data.AppendLine($"firstName: {employee.firstName}");
                    data.AppendLine($"lastName: {employee.lastName}");

                    Console.WriteLine(data.ToString());
                }
                employeeArray = employees.ToArray();
            }
            string host = Environment.GetEnvironmentVariable("HOSTNAME");
            string mysqlHost = Environment.GetEnvironmentVariable("MYSQL_HOST");
            return employeeArray;
        }
    }
}
