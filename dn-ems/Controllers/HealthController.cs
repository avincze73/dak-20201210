using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace dn_ems.Controllers
{
    [Route("/")]
    [ApiController]
    public class HealthController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            string env = Environment.GetEnvironmentVariable("HOSTNAME");
            return new string[] { "{health:ok}"};
        }
    }
}
