# Web Application


## Building the application
```bash
dotnet add package MySql.Data.EntityFrameworkCore
dotnet build
```




## Deploying to kubernetes
```bash
docker-compose build
docker-compose push

kubectl apply -f dn-ems/k8s/mysql-data-persistentvolumeclaim.yaml
kubectl apply -f dn-ems/k8s/db-deployment.yaml
kubectl apply -f dn-ems/k8s/db-service.yaml
kubectl apply -f dn-ems/k8s/k8s.yaml
kubectl logs dn-ems-f7d67564c-qm9nm
kubectl exec -it db-ID -- mysql -u tsuser -ptitkos123
```


 ## Using configmap
 ```bash
kubectl create configmap dn-ems-config --from-literal=EMS_DB=ts
kubectl get configmap dn-ems-config
kubectl describe configmap/dn-ems-config
kubectl edit configmap/dn-ems-config
kubectl scale deployment dn-ems --replicas=0
kubectl scale deployment dn-ems --replicas=1
kubectl apply -f dn-ems-deployment-2.yaml
kubectl logs dn-ems-5ffc4cf754-5fpxf -f
 ```

 ## Using secrets
 ```bash
kubectl create secret generic dn-ems-secrets --from-literal=EMS_PASSWORD=titkos123
kubectl get secret/dn-ems-secrets
kubectl describe secret/dn-ems-secrets
kubectl apply -f dn-ems-deployment-2.yaml
 ```