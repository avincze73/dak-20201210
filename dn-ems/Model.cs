using System;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using MySql.Data.EntityFrameworkCore.DataAnnotations;

namespace dn_ems
{
  public class EmployeeContext : DbContext
  {
    public DbSet<Employee> Employee { get; set; }
   
    protected override void OnConfiguring(DbContextOptionsBuilder options)
    {
        string mysqlHost = Environment.GetEnvironmentVariable("MYSQL_HOSTNAME") == null ? "localhost" : Environment.GetEnvironmentVariable("MYSQL_HOSTNAME");
        string mysqlDatabase = Environment.GetEnvironmentVariable("MYSQL_DB") == null ? "ts" : Environment.GetEnvironmentVariable("MYSQL_DB");
        string mysqlUserName = Environment.GetEnvironmentVariable("MYSQL_USERNAME") == null ? "tsuser" : Environment.GetEnvironmentVariable("MYSQL_USERNAME");
        string mysqlPassword = Environment.GetEnvironmentVariable("MYSQL_PASSWORD") == null ? "titkos123" : Environment.GetEnvironmentVariable("MYSQL_PASSWORD");
        string connectionString = String.Format(@"server={0};database={1};user={2};password={3}",
            mysqlHost, mysqlDatabase, mysqlUserName, mysqlPassword);
        Console.WriteLine(connectionString);    
        options.UseMySQL(connectionString);
    }
        //=> options.UseMySQL("server=127.0.0.1;database=nn;user=nn;password=titkos123");

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      base.OnModelCreating(modelBuilder);

      modelBuilder.Entity<Employee>(entity =>
      {
        entity.HasKey(e => e.id);
        entity.Property(e => e.loginName).IsRequired();
      });

    }
  }


  public class Employee
  {
    public int id { get; set; }
    public string firstName { get; set; }
    public string lastName { get; set; }

    public string loginName { get; set; }

    public string title { get; set; }

    public string password { get; set; }

    public int salary { get; set; }


  }
}
