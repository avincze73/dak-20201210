using System;

namespace dn_currency_conversion
{
    public class CurrencyExchangeModel
    {
        public String Host { get; set; }
        
        public String FromCurrency { get; set; }

        public String ToCurrency { get; set; }

        public double ExchangeRate { get; set; }

    }
}
