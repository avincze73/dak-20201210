using System;

namespace dn_currency_conversion
{
    public class CurrencyConversionModel
    {
        public String Host { get; set; }
        
        public String FromCurrency { get; set; }

        public String ToCurrency { get; set; }

        public int ExchangeRate { get; set; }

        public int Amount { get; set; }

        public int CalculatedValue { get; set; }

    }
}
