using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;

namespace dn_currency_conversion.Controllers
{
    [ApiController]
    [Route("[controller]/from/{from}/to/{to}/amount/{amount}")]
    public class CurrencyConversionController : ControllerBase
    {
        // GET api/values
        [HttpGet]
        public ActionResult<CurrencyConversionModel> GetAsync(string from, string to, int amount)
        {
            string host = Environment.GetEnvironmentVariable("HOSTNAME") == null ? "LOCAL" : Environment.GetEnvironmentVariable("HOSTNAME");

            string currencyExchangeUri = Environment.GetEnvironmentVariable("CURRENCY_EXCHANGE_URI") == null ? "http://localhost" : Environment.GetEnvironmentVariable("CURRENCY_EXCHANGE_URI");
            string currencyExchangePort = Environment.GetEnvironmentVariable("CURRENCY_EXCHANGE_PORT") == null ? "8002" : Environment.GetEnvironmentVariable("CURRENCY_EXCHANGE_PORT");

            string exchangeRate = string.Empty;
            using (var client = new HttpClient())
            {
                string connectionString = String.Format(@"{0}:{1}", currencyExchangeUri, currencyExchangePort);
                Console.WriteLine(connectionString);
                //client.BaseAddress = new Uri(currencyExchangeUri + ":" + currencyExchangePort);
                client.BaseAddress = new Uri(connectionString);
                var responseTask = client.GetStringAsync("currencyexchange/fromm/" + from + "/too/" + to);
                responseTask.Wait();

                var result = responseTask.Result;
                exchangeRate = result;
            }

            int calculatedValue = System.Convert.ToInt32(exchangeRate) * amount;
            return new CurrencyConversionModel{Host = host, 
                    FromCurrency = from,
                    ToCurrency = to,
                    ExchangeRate = System.Convert.ToInt32(exchangeRate), 
                    Amount=amount, 
                    CalculatedValue = calculatedValue};
        }
    }
}
